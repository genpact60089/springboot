package com.genpact.app.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.genpact.app.model.User;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

}
